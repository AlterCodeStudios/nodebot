!!-- NodeBot Changelog <Public> --!!

v1.0 [Ethera] | 10-12-2014 (Updates since early beta releases)
- Administrator, whitelist, and blacklist added (may still be buggy)
- Backend console add (with all the commands)
- Ability to quit the bot inside the console 
- !slap improved on
- !8ball added
- !weather added
- Settings file integrated
- Channel logging added
- Incoming raw messages from IRC server parsed for console
- Version checking added (it won't let you run the bot without the newest version)
- Welcome message added for users that join.

=======