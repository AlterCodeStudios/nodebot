var blacklist = { nickPrefix: '~'; };

blacklist.addNick = function (nick) {
  var defusedNick = blacklist.nickPrefix + nick;
  blacklist[defusedNick] = true;
  return blacklist;
};

blacklist.removeNick = function (nick) {
  var defusedNick = blacklist.nickPrefix + nick;
  delete blacklist[defusedNick];
  return blacklist;
};

blacklist.hasNick = function (nick) {
  var defusedNick = blacklist.nickPrefix + nick;
  return blacklist.hasOwnProperty(defusedNick);
};
 No newline at end of file
