## NodeBot | [MatthewH](http://www.matthewh.in)
#### NodeBot Version - v1.0
### About, Using, and Other Info
#### About NodeBot
NodeBot is a simple IRC bot coded in Node.JS. Made for Freenode, but can work most anywhere. It went originally under the name "MonkeyBlaster", which will explain some of the command names!
##### Warning
This is still an early version of NodeBot, therefore there will be bugs in it. If you find a bug, please report it. 


#### Using NodeBot
To use this bot, first open the settings.json file and edit the information accordingly. Then open your Node.JS terminal and cd into your NodeBot.js directory. Once there run it with `node NodeBot.js`. Then there you go! That's it!


#### Other Info Regarding NodeBot
##### settings.json
Please keep all settings how they are. Do not add or take away. 
If you're curious about the `useWebItems`, all it is doing is not using the local files in `config/IRC ` and using the words/responses from the AlterCodeStudios server (which can have more than the local files). Set this to false if you want to use the local versions.


----
### IRC Commands

`!google`: Searches Google for the choosen term and returns the top link(s).

>Example Command: `!google NodeBot`


`!weather`: Get's the weather for the inputed location.

>Example Command: `!weather Stockholm, Sweden


`!say`: NodeBot says the text after the command.

>Example Command: `!say Hello!`


`!battle`: Use two terms seperated with `vs.` and it will do a Google battle between the two terms.

>Example Command: `!battle Cookies vs. Cake`


`!slap`: NodeBot "slaps" the selected person with a fish.

>Example Command: `!slap Administrator`


`!8ball`: Ask it a question and wait for NodeBot's response.

>Example Command: `!8ball How awesome am I?` 



### Console Commands
`.broadcast`: Broadcasts a message to all channels NodeBot is in.

`.help`: Displays the help message.

`.irc`: Sends commands to NickServ (whatever is typed after the NodeBot terminal command).

`.join`: Joins any specified room/channel on the IRC server.

`.list`: Lists administrators, whitelist, blacklist, or channels.

`.part all`: Leaves all currently joined channels.

`.quit`: Exit NodeBot quietly.


----
### To-Do List


##### Commands
+ Kick (OP required)
+ Define // In progress [Done]
+ Urban // In progress [Done]
+ PM  // In progress [Done]
+ Topic (OP required)
+ Version // In next release [Done]
+ YouTube
+ Mode (silent, whitelist, free) // In progress, subject to change.
+ Poll voting (would be done via PM)
+ Public polls (customizable setting in settings, list polls on specified server)
+ Basic survey system

##### Console Commands
+ WhoIs 
+ Restart // In progress
+ PM user
+ Add to/remove from admins, whitelist, and blacklist. // In progress

##### Features
+ AI based responses in PM
+ Learning ability (if AI is added)

##### Other
+ OP auto check
+ Website? Maybe.
+ Separate directory for servers (simple, will be added later)
+ Full documentation provided with downloads and wiki (possibly)
+ Add vowel recognition for !slap command // In progress [Done]
+ Change vowel recognition to function

##### Completed
+ Say
+ Battle
+ Google
+ Slap
+ 8ball
+ Blacklist
+ Administrator list
+ Whitelist
+ Backend console
+ Quit
+ Chat logging
+ Greetings on join
+ Other commands not listed

----
### Contact
If you would like to contact us for any sort of support, go to my forums [here](http://www.community.matthewh.in)
