// Required libs
var irc = require('irc');
var request = require('request');
var cheerio = require('cheerio');
var google = require('google');
var util = require('util');
var async = require('async');
var fs = require('fs');
var rdl = require('readline');
var momentJS = require('moment');

var configFile = fs.readFileSync('config/settings.json','utf8');
var parsedConfig = JSON.parse(configFile);
var config = {
  server: parsedConfig['server'],
  channels: [],
  botName: parsedConfig['nick'],
  nickservPassword: parsedConfig['password'],
  floodProtectionDelay: parsedConfig['floodDelay'],
};

// Arrays for terminal input.
var administrators = [];
var whitelist = [];
var blacklist = [];
var botChannels = [];

// Terminal config 
var terminalConfig = {
    quitPass: parsedConfig['quitPassword'],
};

var nodebotVersion = 1.0;
request('http://altercodestudios.com/src/NodeBot/version.nodebot', function (error, response, body) {
  if (!error && response.statusCode == 200) {
    var parsedResponse = JSON.parse(body);
    if(nodebotVersion < parsedResponse.currentVersion)
    {
      console.log('There is a new version of NodeBot available. Please download it before continuing.');
      process.exit();
    }
  }
});

var magicBallItems = [];
    if(parsedConfig['useWebItems'] === true)
    {
      request('http://altercodestudios.com/src/NodeBot/!8ball.nodebot', function (error, response, body) {
      if (!error && response.statusCode == 200) {
      //; console.log(body) // Print the google web page.
      //var splitResponse = body.split(/\r?\n/);
      magicBallItems = body.split(/\r?\n/);
      }
    });
    }
    else
    {
      fs.readFileSync('config/IRC/!8ball.nodebot').toString().split('\n').forEach(function (line) {
        magicBallItems.push(line)
      });
    }

var slapItems = [];
    if(parsedConfig['useWebItems'] === true)
    {
      request('http://altercodestudios.com/src/NodeBot/!slap.nodebot', function (error, response, body) {
      if (!error && response.statusCode == 200) {
      slapItems = body.split(/\r?\n/);
      }
    });
    }
    else
    {
      fs.readFileSync('config/IRC/!slap.nodebot').toString().split('\n').forEach(function (line) {
        slapItems.push(line)
      });
    }

// Create and Join
var bot = new irc.Client(config.server, config.botName, {
  channels: config.channels,
  userName: config.botName,
  floodProtection: true,
  floodProtectionDelay: config.floodProtectionDelay,
  debug: false,
  showErrors: true
});

var readAministrators = rdl.createInterface({
    input: fs.createReadStream('config/administrators'),
    output: process.stdout,
    terminal: false
});

readAministrators.on('data', function(data) {
});

fs.readFileSync('config/administrators').toString().split('\n').forEach(function (line) {
    administrators.push(line);
});

fs.readFileSync('config/blacklist').toString().split('\n').forEach(function (line) {
    blacklist.push(line);
});

fs.readFileSync('config/whitelist').toString().split('\n').forEach(function (line) {
   whitelist.push(line);
});

var devInput = rdl.createInterface(process.stdin, process.stdout, devTerminalCompleter), prefix = 'NodeBot Terminal >> ';

function devTerminalCompleter(line)
{
    var completions = '.broadcast .help .irc .join .list .part .quit'.split(' ')
    var hits = completions.filter(function(c) {

    if (c.indexOf(line) === 0) {
    }
  });
  return [hits && hits.length ? hits : completions, line];
}


devInput.on('line', function (cmd) {
    devInput.setPrompt(prefix, prefix.length);
    devInput.prompt();
    
    if(cmd.toLowerCase() == '.help' || cmd.toLowerCase() == 'help' || cmd.toLowerCase() == '?')
    {
      console.log('NodeBot Help <<');
      console.log('.broadcast : Broadcasts a message to all channels ' + bot.nick + ' is in.');
      console.log('.help/help/? : Displays this message.');
      console.log('.irc : Sends commands to NickServ (whatever is typed after the NodeBot terminal command).');
      console.log('.join : Joins any specified room/channel on the IRC server.');
      console.log('.list : Lists administrators, whitelist, blacklist, or channels.');
      console.log('.part : Leaves all currently joined channels.');
      console.log('.quit : Exit NodeBot quietly.');
    }
    else if(cmd.toLowerCase().indexOf('.list') === 0 && numParams(cmd) >= 1)
    {
        var listVar = getParams(cmd).join(' ');
        
        if(listVar.toLowerCase() == 'administrators')
        {
            console.log('Administrators: ' + administrators);
        }
        else if (listVar.toLowerCase() == 'blacklist')
        {
            console.log('Blacklist: ' + blacklist);
        }
        else if (listVar.toLowerCase() == 'whitelist')
        {
            console.log('Whitelist: ' + whitelist);
        }
        else if(listVar.toLowerCase() == 'channels')
        {
            if(botChannels.length >= 1)
            {
                console.log('Current channels: ' + botChannels);
            }
            else 
            {
                console.log(config.botName + ' is not currently part of any channel.');
            }
        }
    }
    else if (cmd.toLowerCase().indexOf('.broadcast') === 0 && numParams(cmd) >= 1)
    {
        var devInputData = getParams(cmd).join(' ');
        bot.say(botChannels, devInputData);
        //console.log(config.botName + ': ' + devInputData);
        console.log(' ');
        NodeResponse(devInputData);
    }
    else if (cmd.toLowerCase().indexOf('.join') === 0 && numParams(cmd) >= 1)
    {
      /* 
              NEED TO FIX ARRAYS! D:
      */
        var devChannel = getParams(cmd).join(' ');
        if (fs.existsSync('ChatLogs/' + devChannel + ".nodebot")) 
        {
          console.log(devChannel + '\'s log already exists. I will just add onto it.');
        }
        else
        {
          console.log(devChannel + '\'s log doesn\'t exist. Creating it now.');
          fs.writeFile('ChatLogs/' + devChannel + '.nodebot', '', function (err) 
          {
            if (err) throw err;
          });
        }
        botChannels.push(devChannel);
        bot.join(devChannel);
        console.log(config.botName + ' has joined ' + devChannel + '.');
    }
    else if (cmd.toLowerCase().indexOf('.part') === 0 && numParams(cmd) >= 1)
    {
        var devPartInput = getParams(cmd).join(' ');
        if(devPartInput == 'all')
        {
            devInput.question('What will your part message be? \n', function(response) {
                bot.part(botChannels, response);
                //botChannels.length = 0;
            });
        }
    }
    else if(cmd.toLowerCase().indexOf('.irc') === 0 && numParams(cmd) >= 1)
    {
        var devIRC = getParams(cmd).join(' ');
        bot.say('NickServ', devIRC);
    }
    else if(cmd.toLowerCase() == '.me')
    {
        //console.log(magicBallItems);
    }
    else if(cmd.toLowerCase() == '.quit')
    {
        devInput.question('What\'s the password? \n', function(response) {
           if(response == terminalConfig.quitPass)
           {
               NodeResponse('Quitting NodeBot. \nGoodbye.');
               process.exit();
           }
           else
           {
               console.log('Wrong password. Retry command.');
           }
        });
    }
});

// Listen to channels
bot.addListener('message#', function (from, to, message) {
    //util.log(from + ' => ' + to + ': ' + message);
    util.log('IRC [' + to + '] >> ' + from + ': ' + message);
    fs.appendFile('ChatLogs/' + to + '.nodebot', '[' + momentJS().format('lll') + '] ' + from + ': ' + message + '\n', function (err) {});
    if(blacklist.indexOf(from) <=0) 
    {
        if (message.toLowerCase().indexOf('!user ') === 0 && numParams(message) >= 1) 
        {
                var searchName = getParams(message).join(' ');
            searchUser(bot, to, searchName);
        }
        else if (message.toLowerCase().indexOf('!docs ') === 0 && numParams(message) >= 1) 
        {
            var name = getParams(message).join(' ');
            searchDocs(bot, to, name);
        }
        else if (message.toLowerCase().indexOf('!google ') === 0 && numParams(message) >= 1 )
        {
            var term = getParams(message).join(' ');
            searchGoogle(bot, to, term);
        }
        else if (message.toLowerCase().indexOf('!battle ') === 0 && numParams(message) >= 1 && message.toLowerCase().indexOf(' vs. ') >= 0) 
        {
            var input = getParams(message).join(' ');
            var terms = input.split(' vs. ');
            if (terms[0] && terms[1]) 
            {
                battle(bot, to, terms[0], terms[1]);
            }
        }
        else if (message.toLowerCase() == '!github') 
        {
            bot.say(to, 'GitHub: ' + links.github);
        }
        else if (message.toLowerCase().indexOf('!github ') === 0 && numParams(message) >= 1) 
        {
            var params = getParams(message);
            github(bot, to, params);
        }
        else if (message.toLowerCase() == '!twitter') 
        {
            bot.say(to, 'Twitter: ' + links.twitter);
        }
        else if (message.toLowerCase() == '!facebook' ) 
        {
            bot.say(to, 'Facebook: ' + links.facebook);
        }
        else if (message.toLowerCase() == '!help') 
        {
            bot.say(from, 'If you need my help, send me a PM with "help"');
        }
        else if (message.toLowerCase().indexOf('!slap ') === 0 && numParams(message) >= 1) 
        {
            var toInsult = getParams(message).join(' ');
            var item = slapItems[Math.floor(Math.random()*slapItems.length)];
            bot.say(to, from + ' slaps ' + toInsult + ' with a(n) ' + item + '.');
        }
        else if (message.toLowerCase().indexOf('!f ') === 0 && numParams(message) >= 1) 
        {
            var toF = getParams(message).join(' ');
            bot.say(to, from + ' f' + toF + ' with awesomeness');
        }
        else if (message.indexOf('!MonkeySay ') === 0 && numParams(message) >= 1) 
        {
            var toSay = getParams(message).join(' ');
            bot.say(to, from + ' says: ' + toSay);
        }
        else if(message.toLowerCase().indexOf('!8ball') === 0 && numParams(message) >= 1)
        {
            var toInsult = getParams(message).join(' ');
            var answer = magicBallItems[Math.floor(Math.random()*magicBallItems.length)];
            bot.say(to, from + ': ' + answer);
        }
        else if (message.toLowerCase().indexOf('!weather ') === 0 && numParams(message) >= 1 && message.toLowerCase().indexOf(', ') >= 0) 
        {
            var weatherInput = getParams(message).join(' ');
            var weatherTerms = weatherInput.split(', ');
            if (weatherTerms[0] && weatherTerms[1]) 
            {
              var arg1 = weatherTerms[0];
              var arg2 = weatherTerms[1];
                request('https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22' + arg1 + '%2C%20' + arg2 +'%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys', function (error, response, body) {
                if (!error && response.statusCode == 200) {
                  var parsedResponse = JSON.parse(body);
                  var degreeType;
                  
                  if(parsedResponse.query.results.channel.units.temperature === 'F')
                  {
                    degreeType = 'fahrenheit';
                  }
                  else 
                  {
                    degreeType = 'celsius';
                  }
                  
                  bot.say(to, parsedResponse.query.results.channel.location.city + ', ' + parsedResponse.query.results.channel.location.region + '\'s weather...');
                  bot.say(to, 'It is currently ' + parsedResponse.query.results.channel.item.condition.temp + ' degrees ' + degreeType + ' and it\'s ' + parsedResponse.query.results.channel.item.condition.text + ' as of ' + parsedResponse.query.results.channel.item.condition.date + '.')
                }});
            }
        }
    }
    if (administrators.indexOf(from) >= 1)
    {
        if(message.toLowerCase() == 'hello')
        {
            bot.say(to, 'Hello to you to ' + from);
        }
        else if (message.toLowerCase().indexOf('!kick ') === 0 && numParams(message) >= 1)
        {
            var userKick = getParams(message).join(' ');
            bot.say(to, 'Kick command run');
            //bot.say(to, '/kick ' + userKick);
            //bot.say(to, userKick + ' has been kicked from ' + to + '.');
        }
    }
});

// Listen to PMs
bot.addListener('pm', function (from, message) {
  util.log(from + ' => ME: ' + message);
  if (message.toLowerCase() == 'help') 
  {
    getHelp(bot, from);
  }
  else if (message.toLowerCase() == 'about') 
  {
    bot.say(from, 'I\'m written in Node.js');
  }
  else if (message.toLowerCase() == 'hello') 
  {
    bot.say(from, 'Hello to you too!');
  }
  else 
  {
    bot.say(from, 'Sorry, I don\'t understand what you want.  Say "help" if you need help.');
  }
});

// On connection
bot.addListener('motd', function(message){
  if (config.nickservPassword !== '') 
  {
    util.log('Recovering nickname');
    recoverNick();
  }
});

// Listeners
bot.addListener('error', function(message) {
  util.log('Error: ', message);
});

bot.addListener('join', function(channel, nick, message)
{
    if(nick != bot.nick)
    {
        bot.say(channel, 'Welcome ' + nick + ' to ' + channel + '. Enjoy your stay!');
    }
});

bot.addListener('part', function(channel, nick, message){
   if(nick != config.botName)
   {
       bot.say(channel, nick + ' has left the channel. (' + message + ')');
   }
});

// Debug response handler
bot.addListener('raw', function (message) {
    //console.log('Raw: ', message);
    try
    {
        var stringify = JSON.stringify(message);
        var stringifyArgs = JSON.stringify(message.args[1]);
        var parsedResponse = JSON.parse(stringify);
        var parsedArgs = JSON.parse(stringifyArgs);
    }
    catch(er){}
    
    try
    {
        //console.log('Parse Prefix >> >> >> ' + pasrsedPrefix["prefix"]);
        if(parsedResponse['nick'] === undefined || parsedArgs === undefined || parsedResponse['command'] === 'PRIVMSG')
        {
            //console.log('NodeBot Error >> Raw message could not be parsed correctly.');
            //console.log('NodeBot Error >> Move along, nothing to see here.');
        }
        else
        {
            console.log('NodeBot IRC >> ' + parsedResponse['nick'] + ': ' + parsedArgs);
            //console.log('NodeBot parsedResponse Args >> >> ' + stringifyArgs);
        }
    }
    catch (er)
    {
        // Error when trying to display message
        console.log('NodeBot Error >> Raw message could not be parsed correctly.');
        //console.log('NodeBot Error >> Move along, nothing to see here.');
    }
    //console.log('stringify >> >> ' + stringify);
    //console.log('JSON Parse Prefix >> >> ' + pasrsedPrefix);
});

///////////////////////////////////////////////////////////////////////////////
// Actions
function NodeResponse(dataResponse)
{
    console.log('NodeBot -> ' + dataResponse);
}

var recoverNick = function() {
  bot.say('NickServ', 'identify ' + config.botName + ' ' + config.nickservPassword);
  /* setTimeout(function() {
    bot.say('NickServ', 'ghost ' + config.botName);
  }, 3000);
  setTimeout(function() {
    bot.say('NickServ', 'release ' + config.botName);
  }, 6000);  
  setTimeout(function() {
    bot.send('NICK', config.botName);
  }, 9000);*/
}

var getHelp = function(bot, to) {
  bot.say(to, 'I respond to the following commands on channels:');
  bot.say(to, '!google [# results] <search term> - searches Google for search term, and returns top result (by default) or up to a maximum of 5 if specified');
  bot.say(to, '!battle <term1> vs. <term2> - does a Google battle with number of results between term1 and term2');
  bot.say(to, '!twitter - links to the my Twitter account');
  bot.say(to, '!facebook - links to the my Facebook page');
  bot.say(to, 'In addition, I respond to the following commands by PM:');
  bot.say(to, 'help - this text you\'re reading');
  bot.say(to, 'about - about me');
}

var searchUser = function(bot, to, searchName) {
  util.log('Look for user: ' + searchName);
 
  // Search the member list, hopefully the user will be somewhere within the first 300 results
  request.post('', { form: { username: searchName, perpage: 300 } }, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      $ = cheerio.load(body);
    
      var usernamesFound = [];
      var found = false;
    
      // Look at all the table rows that have 6 columns, and aren't the first 2 (headers)
      $('tr').each(function(i, e) {
        var numCells = $(this).children('td').toArray().length;
        if (numCells != 6 || i < 2) {
          return;
        }
      
        var userRow = $(this);

        var username = userRow.children('td').eq(1).children('a').eq(0).text()
        usernamesFound.push(username);
      
        if (username.toLowerCase() == searchName.toLowerCase()) {
          // User matched!
          found = true;
        
          var profileLink = userRow.children('td').eq(1).children('a').eq(0).attr('href');
          var postCount = userRow.children('td').eq(4).text();
          var regDate = userRow.children('td').eq(2).text().split(',')[0];
          var lastVisitDate = userRow.children('td').eq(3).text().split(',')[0];
          bot.say(to, username + ': ' + postCount + ' posts on the Community Forums, last visited ' + lastVisitDate + ', member since ' + regDate + '. ' + profileLink);

        }
      
      });
    
      if (!found) 
      {
        if (usernamesFound.length > 0) {
          bot.say(to, 'I couldn\'t find ' + searchName + ', did you mean ' + usernamesFound[Math.floor(Math.random()*usernamesFound.length)] + '?');
        }
        else 
        {
          bot.say(to, 'I couldn\'t find ' + searchName);
        }
      }
    }
  });
}

var searchDocs = function(bot, to, term) {

  // Set number of results per page (it might come from the first word of the term)
  google.resultsPerPage = 1;
  var firstTerm = term.split(' ')[0];
  if (isNumber(firstTerm)) 
  {
    google.resultsPerPage = Math.min(parseInt(firstTerm), 5);
    term = term.split(' ').slice(1).join(' ');
  }
  
  util.log('Search docs for: ' + term + ' and get ' + google.resultsPerPage + ' results');
  
  google(term + ' site:docs.com', function(err, next, links){
    if (err) 
    {
      util.log(err);
      bot.say(to, 'Error fetching search results. Please try again later.');
      return;
    }

    if (links && links.length > 0)
    {
      // We want to show the lesser of what we have, or what we've specified as the limit
      for (var i = 0; i < Math.min(links.length, google.resultsPerPage); ++i) {
        var text = links[i].title;
        if (links[i].link !== null) 
        {
          text += ' - ' + links[i].link;
        }
        bot.say(to, text);
      }
    }
    else 
    {
      bot.say(to, 'No docs results for search term: ' + term);
    }
  });
};

var searchGoogle = function(bot, to, term) {

  // Set number of results per page (it might come from the first word of the term)
  google.resultsPerPage = 1;
  var firstTerm = term.split(' ')[0];
  if (isNumber(firstTerm)) {
    google.resultsPerPage = Math.min(parseInt(firstTerm), 5);
    term = term.split(' ').slice(1).join(' ');
  }
  
  util.log('Search Google for: ' + term + ' and get ' + google.resultsPerPage + ' results');
  
  google(term, function(err, next, links){
    if (err) {
      util.log(err);
      bot.say(to, 'Error fetching search results. Please try again later.');
      return;
    }

    if (links && links.length > 0)
    {
      // We want to show the lesser of what we have, or what we've specified as the limit
      for (var i = 0; i < Math.min(links.length, google.resultsPerPage); ++i) {
        var text = links[i].title;
        if (links[i].link !== null) 
        {
          text += ' - ' + links[i].link;
        }
        bot.say(to, text);
      }
    }
    else 
    {
      bot.say(to, 'No Google results for search term: ' + term);
    }
  });
};

var battle = function(bot, to, term1, term2) {
  util.log('Google battle: ' + term1 + ' vs. ' + term2);
  
  var getNumResults = function (error, response, body, callback) {
    if (!error && response.statusCode == 200) {
      $ = cheerio.load(body);
      var resultsString = $('#resultStats').text();
      if (!resultsString)
      {
        callback(null, 0);
        return;
      }
      var matches = resultsString.match(/ [\d,]+ /);
      console.log(matches[0]);
      var string = matches[0].replace(/,/g, '');
      console.log(string);
      var number = parseInt(matches[0].replace(/,/g, ''));
      console.log(number);
      callback(null, number);
    }
    else {
      callback(error, null);
    }
  };

  async.parallel({
    '1': function (callback) {
      request.get('https://www.google.com/search?q=' + term1, function (error, response, body) { 
        getNumResults(error, response, body, callback);
      });
    },
    '2': function (callback) {
      request.get('https://www.google.com/search?q=' + term2, function (error, response, body) { 
        getNumResults(error, response, body, callback);
      });
    }
  },
  function (error, results) {
    if (error) {
      util.log('Google battle error: ', error);
      bot.say(to, 'Sorry, no referee showed up for this Google battle :(');

    }
    else {
      var winMessage = 'The winner is: '
      if (results['1'] > results['2']) {
        winMessage += term1;
      }
      else if (results['2'] > results['1']) {
        winMessage += term2;
      }
      else {
        winMessage = 'It was a tie!';
      }
      bot.say(to, 'GOOGLE BATTLE: ' + term1 + ' (' + results['1'] + ') vs. ' + term2 + ' (' + results['2'] + ').  ' + winMessage);
    }
  });
};

var github = function(bot, to, params) {
  var repo = params[0], //repo name
      view = params[1], //pull or issue
      id   = params[2], //pull/issue id
      viewCapital = (view) ? view.charAt(0).toUpperCase() + view.slice(1) : null;

  // go through provided parameters and generate an appropriate answer
  if(repo) {
    if(view) {
      if(view == 'pull' || view == 'issue') {
        if(view == 'issue') view = 'issues';

        if(id && isNumber(id)) { //user is requesting link to pull/issue
          bot.say(to, repo + ' ' + viewCapital + ' ' + '#' + id + ': ' + links.github + '/' + repo + '/' + view + '/' + id);
        }
        else {
          bot.say(to, errorMessage);
        }
      }
      else {
        bot.say(to, errorMessage)
      }
    }
    else { //user is requesting repo url
      bot.say(to, repo + ' repository: ' + links.github + '/' + repo);
    }
  }
  else {
    bot.say(to, errorMessage);
  }
};

///////////////////////////////////////////////////////////////////////////////
// Helpers

var numParams = function(text) {
  return text.split(' ').length-1;
};

var getParams = function(text) {
  return text.split(' ').slice(1);
};

var isNumber = function(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
};

///////////////////////////////////////////////////////////////////////////////
// Reusable bits of text

var errorMessage = 'Incorrect and/or missing parameters.';

var links = {
  github: 'http://git.altercodestudios.com/nodebot',
  twitter: 'https://twitter.com/ColdfireTube',
  facebook: 'https://www.facebook.com/ColdfireTube.yt'
};